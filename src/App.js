import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Scene, Router } from 'react-native-router-flux';
import store from './store';
import Home from './containers/HomeScreen';
import Details from './containers/DetailsScreen';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router navigationBarStyle={{ backgroundColor: '#ffffff' }}>
          <Scene key="root">
            <Scene key="homeScreen" component={Home} title="HOME" initial />
            <Scene key="detailsScreen" component={Details} title="DETAILS" />
          </Scene>
        </Router>
      </Provider>
    );
  }
}
