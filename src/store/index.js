import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import rootReducer from '../reducers';


const combineMiddlewares = [promiseMiddleware(), thunkMiddleware];

if (process.env.NODE_ENV === 'development' && __DEV__) {
  const createLoggerMiddleware = require('redux-logger'); // eslint-disable-line global-require

  const loggerMiddleware = createLoggerMiddleware({
    collapsed: true,
    duration: true,
    diff: true
  });

  combineMiddlewares.push(loggerMiddleware);
}

const store = createStore(
  rootReducer,
  applyMiddleware(...combineMiddlewares)
);

export default store;
