export default {
  default: '#55595c',
  dark: '#c6ced5',
  darker: '#2D363D',
  light: '#95A5B2',
  lighter: '#eceeef',
  lightest: '#dadfe4'
};
