import { Dimensions } from 'react-native';

const device = Dimensions.get('window');

export default {
  height: device.height,
  width: device.width
};
