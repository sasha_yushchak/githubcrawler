import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import Spinner from 'react-native-spinkit';

import * as theme from '../theme';

export default class Preloader extends PureComponent {
  render() {
    return (
      <View style={styles.fetching}>
        <Spinner
          style={styles.spinner}
          size={88}
          type={'ThreeBounce'}
          color={spinnerColor}
        />
        <Text style={styles.spinnerText}>LOADING</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 64
  },
  bold: {
    fontWeight: '800'
  },
  fetching: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: theme.colors.darker,
    height: theme.sizes.height,
    width: theme.sizes.width,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.5
  },
  spinner: {
    padding: 30
  },
  spinnerText: {
    fontSize: 20,
    margin: 20,
    textAlign: 'center',
    color: '#ffffff'
  }
});

const spinnerColor = '#ffffff';
