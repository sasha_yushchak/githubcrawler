import React, { PureComponent, PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput
} from 'react-native';
import * as theme from '../theme';


export default class SearchBar extends PureComponent {
  static propTypes = {
    onChangeText: PropTypes.func
  };

  state = {
    value: '',
    focus: false
  };

  _focusInput = () => {
    this._input.focus();
  };

  _onFocus = () => {
    this.setState({ focus: true });
  };

  _onBlur = () => {
    this.setState({ focus: false });
  };

  _onChange = (value) => {
    this.setState({ value });

    if (this.props.onChangeText) {
      this.props.onChangeText(value);
    }
  };

  _renderPlaceholder = () => {
    const { focus, value } = this.state;

    if (focus || value !== '') {
      return;
    }

    return (
      <TouchableWithoutFeedback onPress={this._focusInput}>
        <View style={stylesSearch.placeholderWrapper}>
          <Text style={stylesSearch.placeholder}>Search</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    return (
      <View style={stylesSearch.container}>
        <TextInput
          ref={(c) => {
            this._input = c;
          }}
          keyboardType="default"
          onFocus={this._onFocus}
          onBlur={this._onBlur}
          onChangeText={this._onChange}
          autoCapitalize="none"
          autoCorrect={false}
          keyboardAppearance={'dark'}
          style={stylesSearch.input}
          underlineColorAndroid={'transparent'}
        />
        {this._renderPlaceholder()}
      </View>
    );
  }
}

const stylesSearch = StyleSheet.create({
  container: {
    height: 44,
    justifyContent: 'center',
    backgroundColor: theme.colors.light
  },
  input: {
    alignSelf: 'center',
    height: 28,
    width: theme.sizes.width - 20,
    paddingLeft: 10,
    fontSize: 16,
    backgroundColor: '#ffffff',
    borderRadius: 4,
    color: theme.colors.darker,
    lineHeight: 12
  },
  placeholderWrapper: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  placeholder: {
    paddingLeft: 7,
    fontSize: 13,
    color: '#96A5B1'
  }
});
