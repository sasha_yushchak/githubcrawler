import { combineReducers } from 'redux';
import repositoriesStateReducer from './repositories';

export default combineReducers({
  repositories: repositoriesStateReducer
});
