const initialState = {
  fetching: false,
  data: [],
  page: 1,
  error: null
};

export default repositories = (state = initialState, action) => {
  switch (action.type) {

    case 'REQUEST_REPOSITORIES':
      return {
        ...state,
        fetching: true
      };

    case 'GET_REPOSITORIES_PENDING':
      return {
        ...state,
        fetching: true
      };

    case 'GET_REPOSITORIES_FULFILLED':
      return {
        ...state,
        data: state.data.concat(action.payload),
        page: state.page + 1,
        fetching: false
      };

    case 'GET_REPOSITORIES_ERROR':
      return {
        ...state,
        fetching: true,
        error: action.payload
      };

    case 'SEARCH_REPOSITORIES':
      return {
        ...state,
        fetching: true,
        data: [],
        page: 1
      };

    case 'GET_DETAILS_PENDING':
      return {
        ...state,
        fetching: true
      };

    case 'GET_DETAILS_FULFILLED':
      return {
        ...state,
        details: action.payload,
        fetching: false
      };

    case 'GET_DETAILS_ERROR':
      return {
        ...state,
        fetching: true,
        error: action.payload
      };

    default:
      return state;
  }
};
