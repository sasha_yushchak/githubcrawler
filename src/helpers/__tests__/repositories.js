import repositories from '../../reducers/repositories';
import { initialState } from '../../reducers/repositories';
import * as api  from '../../middlewares/api';
import * as actions from '../../actions/repositories';

it('returns the same state on an unhandled action', () => {
  expect(repositories(initialState, {type: '_NULL'})).toMatchSnapshot();
});

it('handles REQUEST_REPOSITORIES action', () => {
  expect(repositories(initialState, actions.requestRepositories(1, null)))
    .toEqual({
      error: null,
      fetching: false,
      data: [],
      page: 1
    });
});

it('handles SEARCH_REPOSITORIES action', () => {
  expect(repositories(initialState, actions.searchRepositories(1, 'react')))
    .toEqual({
      error: null,
      fetching: false,
      data: [],
      page: 1
    });
});

it('handles GET_DETAILS action', () => {
  const newState = repositories(initialState, actions.requestDetails('react'));
  expect(newState).toMatchSnapshot();
  expect(repositories(newState, actions.requestDetails('react'))).toMatchSnapshot();
});
