import axios from 'axios';

const query = axios.create({
  headers: {
    Authorization: 'token 9f83b2bc8a257db56c835436f26da837acfdb842',
    Accept: 'application/vnd.github.full+json'
  }
});

export function getRepositories(dispatch, page, keyword) {
  const API_ROOT = 'https://api.github.com';
  const API_QUALIFIER = keyword || 'stars:>0';
  const API_REPOSITORIES = `${API_ROOT}/search/repositories?q=${API_QUALIFIER}&sort=stars&order=desc&page=${page}&per_page=20`;

  return query.get(API_REPOSITORIES)
    .then(
      (response) => {
        dispatch({
          type: 'GET_REPOSITORIES_FULFILLED',
          payload: response.data.items
        });
      },
      (error) => {
        dispatch({
          type: 'GET_REPOSITORIES_ERROR',
          payload: error
        });
        throw error; // Rethrow so returned Promise is rejected
      }
    );
}

export function getDetails(dispatch, url) {
  let payload = null;

  return query.get(url)
    .then(
      (response) => {
        const pullsUrl = response.data.pulls_url;
        const pullsQuery = pullsUrl.replace('{/number}', '');
        payload = response.data;

        query.get(pullsQuery)
          .then(
            (responseRequests) => {
              payload.pullRequests = responseRequests.data;
              dispatch({
                type: 'GET_DETAILS_FULFILLED',
                payload
              });
            },
            (error) => {
              dispatch({
                type: 'GET_DETAILS_ERROR',
                payload: error
              });
              throw error; // Rethrow so returned Promise is rejected
            }
          );
      },
      (error) => {
        dispatch({
          type: 'GET_DETAILS_ERROR',
          payload: error
        });
        throw error; // Rethrow so returned Promise is rejected
      }
    );
}
