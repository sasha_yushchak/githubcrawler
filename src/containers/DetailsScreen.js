import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  Button,
  Platform,
  Image,
  ListView
} from 'react-native';
import Preloader from '../components/Preloader';
import { requestDetails } from '../actions';
import * as theme from '../theme';

class DetailsScreen extends Component {
  static propTypes = {
    URL: PropTypes.string.isRequired
  };

  state = {
    ds: new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  };

  componentDidMount() {
    const { URL } = this.props;

    this.props.requestDetails(URL);
  }

  _renderRepo = (data) => {
    const date = Date.parse(data.created_at);
    const dateRaw = new Date(date);

    return (
      <View style={styles.request}>
        <Text style={styles.requestTitle}>
          Created at: { dateRaw.toLocaleString() }
        </Text>
        <Text style={styles.requestTitle} />
        <View style={styles.requestData}>
          <Text style={styles.requestStars}>
            Author: <Text style={styles.bold}>{data.user.login}</Text>
          </Text>
          <Text style={styles.requestStars}>
            Request #<Text style={styles.bold}>{data.number}</Text>
          </Text>
          <Text style={styles.requestStars}>
            Status: <Text style={styles.bold}>{ data.state }</Text>
          </Text>
          <Text style={styles.requestWatchers}>
            Name: <Text style={styles.bold}>{ data.title }</Text>
          </Text>
        </View>
      </View>
    );
  };

  _renderContent = () => {
    const { fetching, details } = this.props;

    if (fetching || !details) {
      return (
        <Preloader />
      );
    }

    const format = (int) => {
      if (Platform.OS === 'ios') {
        return Intl.NumberFormat().format(int);
      }
      return int;
    };
    const avatar = details.organization ? details.organization.avatar_url : null;
    const PR = details.pullRequests.slice(0, 10);

    return (
      <View style={styles.repo}>
        <Text style={styles.repoTitle}>
          Name: <Text style={styles.bold}>{details.name} <Text style={styles.repoIdText}>
          - ID {details.id}</Text></Text>
        </Text>
        <View>
          { avatar && <Image style={styles.avatar} source={{ uri: avatar }} />}
        </View>
        <View style={styles.repoData}>
          <Text style={styles.repoStars}>
            Stars: <Text style={styles.bold}>{ format(details.stargazers_count) }</Text>
          </Text>
          <Text style={styles.repoWatchers}>
            Watchers: <Text style={styles.bold}>{ format(details.watchers_count) }</Text>
          </Text>
          <Text style={styles.repoIssues}>
            Issues: <Text style={styles.bold}>{ format(details.open_issues_count) }</Text>
          </Text>
          <Text style={styles.repoIssues}>
            Forks: <Text style={styles.bold}>{ format(details.forks_count) }</Text>
          </Text>
        </View>
        <ListView
          dataSource={this.state.ds.cloneWithRows(PR)}
          renderRow={(rowData) => this._renderRepo(rowData)}
        />
      </View>
    );
  };

  _renderError = () => {
    const { error } = this.props;

    if (error) {
      return (
        <View style={styles.error}>
          <Text style={styles.errorText}>
            {error.message}
          </Text>
          <Button title="RELOAD" onPress={() => { this.forceUpdate(); }} />
        </View>
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        { this._renderContent() }
        { this._renderError() }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 64,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bold: {
    fontWeight: '800'
  },
  avatar: {
    width: 120,
    height: 120,
    alignSelf: 'center'
  },
  repo: {
    width: theme.sizes.width,
    borderTopWidth: StyleSheet.hairlineWidth * 2,
    backgroundColor: theme.colors.lightest,
    borderColor: theme.colors.lightest
  },
  repoData: {
    padding: 15,
    flexDirection: 'column'
  },
  repoTitle: {
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 14,
    padding: 15
  },
  repoStars: {
    fontWeight: '400',
    fontSize: 12,
    textAlign: 'right'
  },
  repoWatchers: {
    fontWeight: '400',
    fontSize: 12,
    textAlign: 'right'
  },
  repoIssues: {
    fontWeight: '400',
    fontSize: 12,
    textAlign: 'right'
  },


  request: {
    width: theme.sizes.width,
    padding: 10,
    borderTopWidth: StyleSheet.hairlineWidth * 2,
    backgroundColor: '#ffffff',
    borderColor: theme.colors.lightest
  },
  requestData: {
    flexDirection: 'column'
  },
  requestTitle: {
    textAlign: 'left',
    fontWeight: '400',
    fontSize: 14,
    justifyContent: 'space-between'

  },
  requestStars: {
    fontWeight: '400',
    fontSize: 12
  },
  requestWatchers: {
    fontWeight: '400',
    fontSize: 12
  },
  requestIssues: {
    fontWeight: '400',
    fontSize: 12
  },
  error: {
    position: 'absolute',
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
    width: theme.sizes.width,
    height: theme.sizes.height,
    backgroundColor: theme.colors.lightest
  },
  errorText: {
    fontSize: 18,
    color: '#000000'
  }
});

const mapDispatchToProps = ({
  requestDetails
});

const mapStateToProps = (state) => ({
  details: state.repositories.details,
  fetching: state.repositories.fetching,
  error: state.repositories.error
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);
