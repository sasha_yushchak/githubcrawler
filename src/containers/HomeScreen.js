import React, { Component, PropTypes } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  ListView,
  TouchableOpacity,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import throttle from 'lodash/throttle';
import Preloader from '../components/Preloader';
import SearchBar from '../components/SearchBar';
import { requestRepositories, searchRepositories } from '../actions';
import * as theme from '../theme';

class HomeScreen extends Component {
  static propTypes = {
    requestRepositories: PropTypes.func.isRequired,
    searchRepositories: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    repositories: PropTypes.arrayOf(PropTypes.object).isRequired,
    error: PropTypes.string,
    fetching: PropTypes.bool.isRequired

  };

  constructor(props) {
    super(props);
    this.state = {
      keyword: null,
      ds: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
    };
  }

  componentDidMount() {
    this.props.requestRepositories(1);
  }

  componentWillUnmount() {
    this._onSearchThrottled.cancel();
  }

  _onSearch = (search) => {
    search = search.toLowerCase();  // eslint-disable-line no-param-reassign
    this.setState({ keyword: search });

    if (!search.length) {
      this.props.searchRepositories(1);
    } else {
      this.props.searchRepositories(1, search);
    }
  };

  _onSearchThrottled = throttle(this._onSearch, 500);

  _fetchMoreRepositories = () => {
    const { page } = this.props;

    this.props.requestRepositories(page, this.state.keyword);
  };

  _renderRepo = (data) => {
    const format = (int) => {
      if (Platform.OS === 'ios') {
        return Intl.NumberFormat().format(int);
      }
      return int;
    };

    const URL = data.url;

    return (
      <TouchableOpacity onPress={() => { Actions.detailsScreen({ URL }); }} style={styles.repo}>
        <Text style={styles.repoTitle}>
          Name: <Text style={styles.bold}>{data.name}</Text>
        </Text>
        <View style={styles.repoData}>
          <Text style={styles.repoStars}>
            Stars: <Text style={styles.bold}>{ format(data.stargazers_count) }</Text>
          </Text>
          <Text style={styles.repoWatchers}>
            Watchers: <Text style={styles.bold}>{ format(data.watchers_count) }</Text>
          </Text>
          <Text style={styles.repoIssues}>
            Issues: <Text style={styles.bold}>{ format(data.open_issues_count) }</Text>
          </Text>
        </View>
        <View style={styles.repoId}>
          <Text style={styles.repoIdText}>ID {data.id}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  _renderRepoList = () => {
    const { repositories } = this.props;

    if (repositories.length > 0) {
      return (
        <ListView
          dataSource={this.state.ds.cloneWithRows(repositories)}
          renderRow={(rowData) => this._renderRepo(rowData)}
          onEndReached={this._fetchMoreRepositories}
          keyboardShouldPersistTaps
        />
      );
    }
  };

  _renderFetchingLoader = () => {
    const { fetching } = this.props;

    if (fetching) {
      return (
        <Preloader />
      );
    }
  };

  _renderTopBar = () => {
    return (
      <View style={styles.topBar}>
        <Text style={styles.topBarText}>Top Rated GitHub Repositories</Text>
        <SearchBar onChangeText={this._onSearchThrottled} />
      </View>
    );
  };

  _renderError = () => {
    const { error } = this.props;

    if (error) {
      return (
        <View style={styles.error}>
          <Text style={styles.errorText}>
            {error.message}
          </Text>
          <Button title="RELOAD" onPress={() => { this.forceUpdate(); }} />
        </View>
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        { this._renderTopBar() }
        { this._renderRepoList() }
        { this._renderFetchingLoader() }
        { this._renderError() }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 64
  },
  bold: {
    fontWeight: '800'
  },
  topBar: {
    width: theme.sizes.width,
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#ffffff'
  },
  topBarText: {
    fontSize: 20,
    margin: 10,
    textAlign: 'center'
  },
  repo: {
    width: theme.sizes.width,
    padding: 10,
    paddingBottom: 16,
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#ffffff',
    backgroundColor: theme.colors.lightest
  },
  repoId: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderWidth: StyleSheet.hairlineWidth * 1,
    borderBottomLeftRadius: 8,
    paddingVertical: 4,
    paddingHorizontal: 8
  },
  repoIdText: {
    color: theme.colors.dark
  },
  repoData: {
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  repoTitle: {
    textAlign: 'left',
    fontWeight: '400',
    fontSize: 14,
    marginBottom: 6
  },
  repoStars: {
    fontWeight: '400',
    fontSize: 12,
    textAlign: 'right'
  },
  repoWatchers: {
    fontWeight: '400',
    fontSize: 12,
    textAlign: 'right'
  },
  repoIssues: {
    fontWeight: '400',
    fontSize: 12,
    textAlign: 'right'
  },
  error: {
    position: 'absolute',
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
    width: theme.sizes.width,
    height: theme.sizes.height,
    backgroundColor: theme.colors.lightest
  },
  errorText: {
    fontSize: 18,
    color: '#000000'
  }
});

const mapDispatchToProps = ({
  requestRepositories,
  searchRepositories
});

const mapStateToProps = (state) => ({
  fetching: state.repositories.fetching,
  repositories: state.repositories.data,
  page: state.repositories.page,
  error: state.repositories.error
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
