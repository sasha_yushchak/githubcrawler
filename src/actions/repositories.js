import * as apiMiddleware from '../middlewares/api';

export function requestRepositories(page, keyword) {
  return (dispatch) => {
    dispatch({
      type: 'REQUEST_REPOSITORIES'
    });

    apiMiddleware.getRepositories(dispatch, page, keyword);
  };
}

export function searchRepositories(page, keyword) {
  return (dispatch) => {
    dispatch({
      type: 'SEARCH_REPOSITORIES'
    });

    apiMiddleware.getRepositories(dispatch, page, keyword);
  };
}

export function requestDetails(url) {
  return (dispatch) => {
    dispatch({ type: 'GET_DETAILS_PENDING' });

    apiMiddleware.getDetails(dispatch, url);
  };
}
