const dev = {
};

const prod = { ...dev };

if (!__DEV__) {
  /* eslint-disable no-console */
  console.log =
  console.info =
  console.error =
  console.warn =
  console.debug =
  console.trace = () => {};
  /* eslint-enable no-console */
}

export default __DEV__ ? dev : prod;
